@extends('index')

@section('breadcrumbs', Breadcrumbs::render('type', $id))

@section('table')

    <div class="subpart-table">
        <h1 align="center">Subparts</h1>
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th>Subpart Name</th>
                <th>Quantity</th>
                <th>Last Purchased</th>
                <th>Buy Count</th>
                <th>Company</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody id="mainSubpartBody">
            @foreach ($data as $i)
                <tr data-id= {{ $i->id }}>
                    <td>
                        <div contenteditable>  {{ ucfirst($i->name) }} </div>
                    </td>
                    <td width="20%">
                        <div contenteditable>  {{ $i->quantity }} </div>
                    </td>
                    <td width="20%">
                        <div contenteditable> {{ $i->last_purchased }} </div>
                    </td>
                    <td width="20%">
                        <div contenteditable> {{ $i->buy_count }} </div>
                    </td>
                    <td width="20%">
                        <div contenteditable> {{ $i->company }} </div>
                    </td>
                    <td width="6%"><button data-id= {{ $i["id"] }} class="btn btn-info subparts-button" >
                        Subparts</button></td>
                    <td width="6%"><button data-id= {{ $i->id }} data-belongsto= {{ $i->name }}
                                           class="btn btn-danger type-delete" data-toggle="modal"
                                           data-target="#deleteModal">Delete</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


@stop