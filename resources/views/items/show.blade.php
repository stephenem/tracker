@extends('index')

@section('breadcrumbs', Breadcrumbs::render('item', $id))

@section('table')

    <div class="type-table">
        <h1 align="center">Types</h1>
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th>Type Name</th>
                <th>Quantity</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody id="mainTypeBody">
            @foreach ($data as $i)
                <tr data-id= {{ $i->id }}>
                    <td>
                        <div contenteditable>  {{ ucfirst($i->name) }} </div>
                    </td>
                    <td width = "20%">
                        <div contenteditable>  {{ $i->quantity }} </div>
                    </td>
                    <td width="6%"><button data-id= {{ $i["id"] }} class="btn btn-info subparts-button" >
                                    Subparts</button></td>
                    <td width="6%"><button data-id= {{ $i->id }} data-belongsto= {{ $i->name }}
                                           class="btn btn-danger type-delete" data-toggle="modal"
                                           data-target="#deleteModal">Delete</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@stop

@section('modal')

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(array('class' => 'form-horizontal', 'id' => 'addTypeForm')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Type</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-group">
                            <div class="col-sm-10">
                                {!! Form::text('name', null, ['class' => 'form-control',
                                'id' => 'typemName',
                                'placeholder' => 'Enter type name']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input class="form-control col-sm-9" name="quantity" placeholder="Enter quantity"
                                       type="number" min="0" max="100"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop

@section('delete_modal')

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="deleteModalLabel">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <p class="lead" id="deleteMessage"/>
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteModalSave">
                        Confirm</button>
                </div>
            </div>
        </div>
    </div>

@stop

@section('custom_js')
    <script src="{{asset('js/type.js')}}"></script>
@stop

