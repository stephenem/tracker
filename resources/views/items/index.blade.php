@extends('index')

@section('breadcrumbs', Breadcrumbs::render('home'))

@section('table')

    <div class="item-table">
        <h1 align="center">Main Items</h1>
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th>Item Name</th>
                <th>Quantity</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody id="mainItemBody">
            @foreach ($data as $i)
                <tr data-id= {{ $i->id }}>
                    <td>
                        <div class="itemName" contenteditable>{{ ucfirst($i->name) }}</div>
                    </td>
                    <td width = "20%">
                        <div contenteditable>  {{ $i->quantity }} </div>
                    </td>
                    <td width="6%"><button data-id= {{ $i->id }} class="btn btn-info types-button">Types</button></td>
                    <td width="6%"><button data-id= {{ $i->id }} data-belongsto= {{ $i->name }}
                                           class="btn btn-danger item-delete" data-toggle="modal"
                                           data-target="#deleteModal">Delete</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@stop

@section('modal')

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(array('url' => '/item', 'id' => 'addItemForm')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Item</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-group">
                            <div class="col-sm-9">
                                {!! Form::text('name', null, ['class' => 'form-control',
                                'id' => 'itemName',
                                'placeholder' => 'Enter item name']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        @stop

        @section('delete_modal')

            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="deleteModalLabel">Confirm Delete</h4>
                        </div>
                        <div class="modal-body">
                            <p class="lead" id="deleteMessage"/>
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="deleteModalSave">
                                Confirm</button>
                        </div>
                    </div>
                </div>
            </div>

        @stop

        @section('custom_js')

            <script src="{{asset('js/item.js')}}"></script>

@stop