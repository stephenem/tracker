<?php namespace TrackerApp;
/**
 * Created by PhpStorm.
 * User: Stephen Em
 * Date: 5/14/2015
 * Time: 2:35 PM
 */

use Illuminate\Database\Eloquent\Model;

class Subpart extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subparts';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'quantity', 'last_purchased','buy_count', 'company', 'type_id', 'superpart_id'];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function types() {
        return $this->belongsTo('TrackerApp\Type');
    }

    public function superpart() {
        return $this->belongsTo('TrackerApp\Subpart');
    }

    public function subparts() {
        return $this->hasMany('TrackerApp\Subpart');
    }
}