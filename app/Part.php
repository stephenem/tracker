<?php namespace TrackerApp;

class Part extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'parts';

    public $timestamps = false;


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id', 'item_id', 'quantity', 'date', 'buy_count', 'company'];
}
