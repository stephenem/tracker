<?php namespace TrackerApp;

use Illuminate\Database\Eloquent\Model;

class Type extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'types';

    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'item_id', 'quantity'];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function item()
    {
        return $this->belongsTo('TrackerApp\Item');
    }

    public function subparts() {
        return $this->hasMany('TrackerApp\Subpart');
    }
}
