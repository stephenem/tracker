<?php namespace TrackerApp\Http\Controllers;

use TrackerApp\Http\Requests;
use TrackerApp\Http\Controllers\Controller;
use \TrackerApp\Type as Type;
use \TrackerApp\Item as Item;

use Illuminate\Http\Request;

class ItemController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $items = Item::all();

        return view('items.index', [
            'data' => $items
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $item = Item::firstOrCreate(['name' => strtolower($request->input('name'))]);
        return $item;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $types = Type::where('item_id', '=', $id)->get();

        return view('items.show', [
            'id'   => $id,
            'data' => $types
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $item = Item::find($id);
        $item->delete();
        return "deleted the item";
	}

}
