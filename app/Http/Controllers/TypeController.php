<?php namespace TrackerApp\Http\Controllers;

use TrackerApp\Http\Requests;
use TrackerApp\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use \TrackerApp\Type as Type;
use \TrackerApp\Item as Item;

use Illuminate\Http\Request;

class TypeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $item_id = $request->route('itemId');
        $type = Type::firstOrCreate(['name' => strtolower($request->input('name')),
                                     'quantity' => $request->input('quantity'),
                                     'item_id' => $item_id
        ]);

        $updated_quantity = Type::where('item_id', '=', $item_id)->sum('quantity');
        Item::find($item_id)->increment('quantity', $request->input('quantity'));
        return $type;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
        $subparts = Type::find($request->route('typeId'))->subparts;
        return view('types/show', [
            'id' => $request->route('typeId'),
            'data' => $subparts
        ]);
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
        $type = Type::find($request->route('typeId'));
        Item::find($request->route('itemId'))->decrement('quantity', $type->quantity);
        $type->delete();

        return 'deleted type';
	}

}
