<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// Routes for Item
Route::get('/', ['as' => 'home', 'uses' => 'ItemController@index']);
Route::post('item', 'ItemController@store');
Route::get('item/{itemId}', ['as' => 'item', 'uses' => 'ItemController@show']);
Route::delete('item/{itemId}', 'ItemController@destroy');

// Routes for Type
Route::get('item/{itemId}/type/{typeId}', ['as' => 'type', 'uses' => 'TypeController@show']);
Route::post('item/{itemId}/type', ['as' => 'typeStore', 'uses' => 'TypeController@store']);
Route::delete('item/{itemId}/type/{typeId}', ['as' => 'typeDelete', 'uses' => 'TypeController@destroy']);

// Routes for Subpart
Route::get('subpart/{subpartId}', function() {
    return 'You got here';
});