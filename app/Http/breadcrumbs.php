<?php

use \TrackerApp\Type as Type;
use \TrackerApp\Item as Item;
use \TrackerApp\Subpart as Subpart;

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('home'));
});

// Home > [Item]
Breadcrumbs::register('item', function($breadcrumbs, $id)
{
    $item = Item::findOrFail($id);
    $breadcrumbs->parent('home');
    $breadcrumbs->push(ucfirst($item->name), route('item', $id));
});

// Home > [Item] > [Type]
Breadcrumbs::register('type', function($breadcrumbs, $id)
{
    $type = Type::findOrFail($id);
    $breadcrumbs->parent('item', $type->item_id);
    $breadcrumbs->push(ucfirst($type->name), route('type', $id));
});

Breadcrumbs::register('superpart', function($breadcrumbs, $subpart)
{
    $superpart = Subpart::findOrFail($subpart);
    if ($subpart>parent)
        $breadcrumbs->parent('category', $category->parent);
});