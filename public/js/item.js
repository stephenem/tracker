$(document).ready(function() {

    $('#homeButton').click(function () {
        document.location.href="/";
    });

    $('.types-button').click(function() {
        var item_id = $(this).data('id');
        window.location.href = '/item/' + item_id;
    });

    $('#addItemForm').submit( function(e) {
        console.log("in here");
        var postData = $(this).serializeArray(),
            formURL = $(this).attr("action");
        $.ajax({
            method: 'POST',
            url: formURL,
            data: postData,
            success: function (data, textStatus, jqXHR) {
                console.log(data, textStatus);
                var rows = $('<tr data-id=' + data.id + '> \
                             <td><div class="itemName" contenteditable>' + data.name + '</td> \
                             <td width="20%"><div contenteditable>0</div></td>\
                             <td width="6%"><button data-id=' + data.id + ' class="btn btn-info types-button">\
                                            Types</button></td> \
                             <td width="6%"><button class="btn btn-danger item-delete" data-toggle="modal" data-id='
                                                    + data.id + ' data-belongsto=' + data.name + ' ' +
                                                    'data-target="#deleteModal">Delete</button></td> \
                         	</tr>');
                rows.hide();
                $('tbody:last').append(rows);
                rows.fadeIn("slow");
                $('#addModal').modal('hide');
            }
        });
        e.preventDefault(); //STOP default action
    });

    $('#mainItemBody').on("click", '.item-delete', function() {
        var item = $(this).data('belongsto'),
            id   = $(this).data('id');
        console.log(id);
        $('#deleteMessage').html('You are about to DELETE "' + item + '"');
        $('#deleteModalSave').attr('data-id',  id);
    });

    $('#deleteModalSave').click(function() {
        var item_id = $(this).attr('data-id');
        console.log(item_id);
        $.ajax({
            method: 'DELETE',
            url: '/item/' + item_id,
            data: {
                '_token' : $('input[name=_token]').val()
            },
            success: function(result) {
                $("tr[data-id="+item_id+"]").fadeOut();
                console.log(result);
            }
        });
    });

});