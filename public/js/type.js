$(document).ready(function() {

    $('#homeButton').click(function () {
        document.location.href="/";
    });

    $('.subparts-button').click(function() {
        var type_id = $(this).data('id');
        //window.location.href = '/item/' + item_id;
    });

    $('#addTypeForm').submit( function(e) {
        console.log("in here");
        var postData = $(this).serializeArray(),
            formURL = $(this).attr("action");
        $.ajax({
            method: 'POST',
            url: formURL + '/type',
            data: postData,
            success: function (data, textStatus, jqXHR) {
                console.log(data, textStatus);
                var rows = $(
                    '<tr data-id=' + data.id + '> \
                        <td><div class="itemName" contenteditable>' + data.name + '</td> \
                        <td width="20%"><div contenteditable>0</div></td>\
                        <td width="6%"><button data-id=' + data.id + ' class="btn btn-info types-button">\
                                            Subparts</button></td> \
                        <td width="6%"><button class="btn btn-danger type-delete" data-toggle="modal" data-id='
                                                    + data.id + ' data-belongsto=' + data.name + ' ' +
                                                    'data-target="#deleteModal">Delete</button></td> \
                    </tr>');
                rows.hide();
                $('tbody:last').append(rows);
                rows.fadeIn("slow");
                $('#addModal').modal('hide');
            }
        });
        e.preventDefault(); //STOP default action
    });

    $('#mainTypeBody').on("click", '.type-delete', function() {
        var item = $(this).data('belongsto'),
            id   = $(this).data('id');
        console.log(id);
        $('#deleteMessage').html('You are about to DELETE "' + item + '"');
        $('#deleteModalSave').attr('data-id',  id);
    });

    $('#deleteModalSave').click(function() {
        var type_id = $(this).attr('data-id');
        console.log(type_id);
        $.ajax({
            method: 'DELETE',
            url: window.location.href + '/type/' + type_id,
            data: {
                '_token' : $('input[name=_token]').val()
            },
            success: function(result) {
                $("tr[data-id="+type_id+"]").fadeOut();
                console.log(result);
            }
        });
    });

});