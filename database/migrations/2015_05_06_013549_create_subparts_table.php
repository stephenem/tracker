<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubpartsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('subparts')) {
            Schema::create('subparts', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->integer('quantity')->default(0);
                $table->date('last_purchased');
                $table->integer('buy_count');
                $table->string('company');
                $table->integer('type_id')->references('id')->on('types');
                $table->integer('superpart_id')->references('id')->on('subparts');
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('subparts');
	}

}
