<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubpartsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Get type id
        $table_id = DB::table('types')->where('name', 'round table')->pluck('id');

        DB::table('subparts')->insert([
            ['name' => 'table legs', 'type_id' => $table_id, 'company' => 'www.tables.com', 'buy_count' => 0],
        ]);
    }

}
