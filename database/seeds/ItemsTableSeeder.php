<?php
/**
 * Created by PhpStorm.
 * User: Stephen Em
 * Date: 5/5/2015
 * Time: 10:29 PM
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ItemsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->delete();

        DB::table('items')->insert([
            ['name' => 'table'],
            ['name' => 'chair']
        ]);
    }

}
