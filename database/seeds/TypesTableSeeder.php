<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TypesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Get type id's
        $table_id = DB::table('items')->where('name', 'table')->pluck('id');
        $chair_id = DB::table('items')->where('name', 'chair')->pluck('id');

        DB::table('types')->insert([
            ['name' => 'round table', 'item_id' => $table_id, 'quantity' => 10],
            ['name' => 'rectangle table', 'item_id' => $table_id, 'quantity' => 10],
            ['name' => 'rolling chair', 'item_id' => $chair_id, 'quantity' => 20]
        ]);

        // Update quantity to reflect new types
        $table_quantity = DB::table('types')
                                        ->select(DB::raw('SUM(`quantity`) as total'))
                                        ->where('item_id', '=', $table_id)
                                        ->get();
        $chair_quantity = DB::table('types')
                                        ->select(DB::raw('SUM(`quantity`) as total'))
                                        ->where('item_id', '=', $chair_id)
                                        ->get();
        DB::table('items')->where('id', $table_id)->update(['quantity' => $table_quantity[0]->total]);
        DB::table('items')->where('id', $chair_id)->update(['quantity' => $chair_quantity[0]->total]);
    }

}
